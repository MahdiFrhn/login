import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Home, Setting} from './screens';
import routes from './routes';


export default () => (
  <Router >
    {routes.map((i) => (
      <Route key={i.path} path={i.path} component={i.component} exact />
    ))}
  </Router>
);
