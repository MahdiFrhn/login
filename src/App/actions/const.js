import { create } from 'axios';
export const BASE_URL = 'https://reqres.in/api';

export const API = create({
  baseURL: BASE_URL,
  headers: { Authorization: 'Bearer ' + localStorage.token },
});

