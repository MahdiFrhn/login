import SC from './screens';

export default [
  {
    path: '/',
    component: SC.Home,
  },
  {
    path: '/setting',
    component: SC.Setting,
  },
  {
    path: '/NotFound',
    component: SC.NotFound,
  },
  {
    path: '/Login',
    component: SC.Login,
  },
  {
    path: '/Register',
    component: SC.Register,
  },
];
