import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Grid } from '@material-ui/core';
class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const user = localStorage.getItem('token');
    return (
      <Container maxWidth="xs" style={{ marginTop: 100 }}>
        <Grid
          container
          spacing={1}
          direction="column"
          justify="center"
          alignItems="center"
          alignContent="stretch"
          >
          <Button variant="contained" color="secondary">
            Log out
          </Button>
          <h3>token is : {user}</h3>
        </Grid>

        
      </Container>
    );
  }
}

export default index;
