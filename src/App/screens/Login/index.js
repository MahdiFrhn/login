import React from 'react';
import {
  Avatar,
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
  Typography,
  Container,
} from '@material-ui/core';

import Styles from './styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Alert from '@material-ui/lab/Alert';
import { Login } from '../../actions';

class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      message: false,
    };
  }

  handleLogin = () => {
    let req = {
      email: this.state.email,
      password: this.state.password,
    };
    Login(req)
      .then((res) => {
        console.log('res', res);
        if (res.status === 200) {
          localStorage.setItem('token', res.data.token);
          this.setState({ message: true });
          setTimeout(() => {
            this.props.history.push('/')
          }, 3000);
          
        }
      })
      .catch((err) => {
        if (err.response) {
          alert('User Not Found');
        }
      });
  };

  render() {
    return (
      <Container  maxWidth="xs">
        {this.state.message ? (
          <Alert variant="filled" severity="success">
            Login is success — Welcome!
          </Alert>
        ) : null}

        <div className={Styles.paper}>
          <Avatar className={Styles.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={Styles.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              value={this.state.email}
              onChange={(e) => this.setState({ email: e.target.value })}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={this.state.password}
              onChange={(e) => this.setState({ password: e.target.value })}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
            
              fullWidth
              variant="contained"
              color="primary"
              onClick={this.handleLogin}>
              Sign In
            </Button>
          </form>
        </div>
      </Container>
    );
  }
}

export default index;
