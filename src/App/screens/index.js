import Home from './Home';
import NotFound from './NotFound';
import Setting from './Setting';
import Login from './Login';
import Register from './Register';

export default {
  Home,
  Setting,
  NotFound,
  Login,
  Register,
};

export { Home, Setting, NotFound, Login, Register };
